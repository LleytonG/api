quart # Quart Microframework
cerberus # Validation library
asyncio # AsyncIO
aiohttp # AsyncHTTP
asyncpg # Asynchronous PostgreSQL library
bcrypt # duh
itsdangerous # Used to sign tokens
minio # S3 Client
sendgrid # e-mail client