from quart import Blueprint, Response
from quart.wrappers import response
from quart import current_app as app
from quart import request, jsonify

from .. import storage

from ..common import identifiers
from ..common.auth import check_token
from ..common.uploads import write_file_to_db, check_bucket

from ..errors import FailedUpload

bp = Blueprint("upload", __name__)


@bp.route("/upload", methods=["GET"])
async def upload_methods_list():
    return jsonify(
        {
            "available": {
                "/upload": "soonTM",
                "/upload/pomf": "POMF-spec compatible upload endpoint. ?key, ?token, or the Authorization header are supported as authentication methods.",
            }
        }
    )


@bp.route("/upload/pomf", methods=["POST"])
async def upload_pomf():
    user = await storage.get_user(identifier=await check_token(request))

    file = (await request.files).get("files[]")
    length = len(file.stream.getvalue())

    maximum_file_size = user.get(
        "maximum_upload_size", app.api_config.DEFAULT_UPLOAD_LIMIT * 1024 * 1024
    )

    if not length <= maximum_file_size:
        raise FailedUpload(
            f"File with size {0} exceeds maximum file size of {1}".format(
                length, maximum_file_size
            )
        )

    file_extension = (
        ".no_extension_when_uploading"
        if file.filename.count(".") == 0
        else "." + file.filename.split(".")[-1]
    )
    file_name = await identifiers.file_name() + file_extension

    bucket = await check_bucket(request)

    await write_file_to_db(
        user, await identifiers.file_id(), file_name, file.filename, length
    )

    app.minio.put_object(
        bucket_name=bucket,
        object_name=file_name,
        data=file,
        length=length,
        content_type=file.content_type,
    )

    return jsonify(
        {
            "file": {
                "name": file.filename,
                "length": length,
                "type": file.content_type,
            },
            "url": file_name,
        }
    )
