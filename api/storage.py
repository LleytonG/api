from quart import current_app as app
from .errors import APIError


async def get_user(id_type="id", identifier=None, context=""):
    """Fetches a user by the given identifier.
    Args:
        id_type: The type of identifier given (Valid: [id, username, email])
        identifier: The identifier that matches the given id_type
        context: What kind of context should we deliver? (Valid: [auth, empty string])
    Returns:
        dict: The user information according to the given context
    """
    # Turn to lowercase for uniformity and cleanliness.
    id_type = id_type.lower()
    context = context.lower()

    if id_type == "id":
        if context == "auth":
            user = await app.db.fetchrow(
                "SELECT id, username, email, password, verified, banned, admin FROM users WHERE id = $1",
                identifier,
            )

            if not user:
                return None

            return {
                "id": user["id"],
                "username": user["username"],
                "email": user["email"],
                "password": user["password"],
                "verified": user["verified"],
                "banned": user["banned"],
                "admin": user["admin"],
            }
        else:
            user = await app.db.fetchrow(
                "SELECT id, username, email, verified, banned, admin FROM users WHERE id = $1",
                identifier,
            )

            if not user:
                return None

            return {
                "id": user["id"],
                "username": user["username"],
                "email": user["email"],
                "verified": user["verified"],
                "banned": user["banned"],
                "admin": user["admin"],
            }

    elif id_type == "username":
        if context == "auth":
            user = await app.db.fetchrow(
                "SELECT id, username, email, password, verified, banned, admin FROM users WHERE username = $1",
                identifier,
            )

            if not user:
                return None

            return {
                "id": user["id"],
                "username": user["username"],
                "email": user["email"],
                "password": user["password"],
                "verified": user["verified"],
                "banned": user["banned"],
                "admin": user["admin"],
            }
        else:
            user = await app.db.fetchrow(
                "SELECT id, username, email, verified, banned, admin FROM users WHERE username = $1",
                identifier,
            )

            if not user:
                return None

            return {
                "id": user["id"],
                "username": user["username"],
                "email": user["email"],
                "verified": user["verified"],
                "banned": user["banned"],
                "admin": user["admin"],
            }

    elif id_type == "email":
        if context == "auth":
            user = await app.db.fetchrow(
                "SELECT id, username, email, password, verified, banned, admin FROM users WHERE email = $1",
                identifier,
            )

            if not user:
                return None

            return {
                "id": user["id"],
                "username": user["username"],
                "email": user["email"],
                "password": user["password"],
                "verified": user["verified"],
                "banned": user["banned"],
                "admin": user["admin"],
            }
        else:
            user = await app.db.fetchrow(
                "SELECT id, username, email, verified, banned, admin FROM users WHERE email = $1",
                identifier,
            )
            if not user:
                return None

            return {
                "id": user["id"],
                "username": user["username"],
                "email": user["email"],
                "verified": user["verified"],
                "banned": user["banned"],
                "admin": user["admin"],
            }

    else:
        raise APIError(
            "There is no code set in storage.py to handle identifier type of `{0}`".format(
                id_type
            )
        )


async def get_user_uploads(user_id):
    """
    Gets all of the user's uploads.
    Args:
        user_id (str): The user's id
    Returns:
        list: All of the user's uploaded files ids
    """
    upload_ids = await app.db.fetch("SELECT id FROM files WHERE user_id = $1", user_id)
    return list(map(lambda file: file["id"], upload_ids))


async def get_users(constraint="all"):
    """Gets all users based on the given constraint.
    Args:
        constraint (str): The constraint (oneof: active, inactive, banned, *: (none))
    Returns:
        list: All the registered users and their metadata according to non-contextual get_user
    """
    users_list = []
    constraint = constraint.lower()

    if constraint == "active":
        users = await app.db.fetch("SELECT id FROM users WHERE verified = true")
    elif constraint == "inactive":
        users = await app.db.fetch("SELECT id FROM users WHERE verified = false")
    elif constraint == "banned":
        users = await app.db.fetch("SELECT id FROM users WHERE banned = true")
    else:
        users = await app.db.fetch("SELECT id FROM users")

    for db_user in users:
        user = await get_user(identifier=db_user["id"])

        users_list.append(user)

    return users_list


async def get_user_id_by_username(username):
    """Gets the ID of a user based on the user's username.
    Args:
        username: The user's username.
    Returns:
        string: The ID of the user from the database.
    """
    return await app.db.fetchval("SELECT id FROM users WHERE username = $1", username)


async def get_user_id_by_email(email):
    """Gets the ID of a user based on the user's email.
    Args:
        email: The user's username.
    Returns:
        string: The ID of the user from the database.
    """
    return await app.db.fetchval("SELECT id FROM users WHERE email = $1", email)
