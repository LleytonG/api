import re

from ..common import identifiers
from ..errors import FailedLogin
from .. import storage
from ..common.email import send_mail

from quart import jsonify
from quart import current_app as app

from itsdangerous import TimestampSigner

import bcrypt


async def user_exists(email, username):
    """Checks if a user with the given email or username exists."""
    if (
        await app.db.fetchval(
            "SELECT id FROM users WHERE email = $1 OR username = $2", email, username
        )
    ) is not None:
        return True

    return False


async def check_credentials(username, password):
    """Validates the password given by the user against the hashed entry in the database
    Args:
        username: The user's username used for authentication.
        password: The password the user gave us.
    Returns:
        bool: Whether the password is correct or not.
    """
    user = await storage.get_user(
        id_type="username", identifier=username, context="auth"
    )
    return bcrypt.checkpw(password.encode("utf-8"), user["password"].encode("utf-8"),)


async def create_user(request):
    """Creates a user in the database.
    Args:
        request: Takes in the entire request object.
    Returns:
        Response: Response object through JSONify.
    """
    user = {
        "id": await identifiers.user_id(),
        "username": request.get("username"),
        "email": request.get("email"),
        "password": await hash_password(request.get("password")),
    }

    await app.db.execute(
        "INSERT INTO users (id, username, email, password) VALUES ($1, $2, $3, $4)",
        user.get("id"),
        user.get("username"),
        user.get("email"),
        user.get("password"),
    )

    if not app.api_config.REGISTRATIONS_REQUIRE_APPROVAL:
        await send_mail(user, "verification")
    else:
        await send_mail(user, "signup_admin_approval_required")

    return jsonify(
        {
            "success": True,
            "message": "Your account with userid ({0}) has been created. Please check your email to verify your account.".format(
                user.get("id")
            ),
        }
    )


async def verify_user(user_id):
    return await app.db.execute(
        "UPDATE users SET verified = true WHERE id = $1", user_id
    )


async def delete_unverified_user(user_id):
    return await app.db.execute("DELETE FROM users WHERE id = $1", user_id)


async def update_password(user, new_password):
    new_password = await hash_password(new_password)
    return await app.db.execute(
        "UPDATE users SET password = $1 WHERE id = $2", new_password, user.get("id")
    )


async def hash_password(password):
    return bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt()).decode("utf-8")


# Token mechanism // A lot of this was inspired by elixire's codebase, as to implement stateless authentication.
async def create_token(user_id, type="timed"):
    """Creates an authentication token.
    Args:
        user_id (str): The ID of the user.
        type (str): Whether the token should expire or not. (Valid types: timed, nontimed, verification)
    Returns:
        string: The authentication token.
    """
    user = await storage.get_user(identifier=user_id, context="auth")

    s = TimestampSigner(
        app.api_config.TOKEN_SIGNING_KEY, salt=user["password"].encode("utf-8")
    )

    if type == "nontimed":
        return s.sign(f"u.{user_id}").decode("utf-8")
    elif type == "verification":
        return s.sign(f"v.{user_id}").decode("utf-8")
    elif type == "unverified_deletion_token":
        return s.sign(f"d.{user_id}").decode("utf-8")
    elif type == "password_reset_token":
        return s.sign(f"p.{user_id}").decode("utf-8")
    return s.sign(f"{user_id}").decode("utf-8")


async def get_token(request):
    """Fetches the authentication token from the request."""
    try:
        return request.args["token"]
    except KeyError:
        return request.headers["Authorization"]


async def check_token(request):
    try:
        token = await get_token(request)

        assert token
    except (TypeError, KeyError, AssertionError):
        raise FailedLogin("No token provided.")

    data = token.split(".")

    is_upload_token = data[0] == "u"
    is_verification_token = data[0] == "v"
    is_password_reset_token = data[0] == "p"
    is_unverified_deletion_token = token[:2] == "d."

    if (
        is_upload_token
        or is_verification_token
        or is_unverified_deletion_token
        or is_password_reset_token
    ):
        # If the token is used as an uploader or verification token, select the right part as the user id.
        user_id = data[1]
    else:
        user_id = data[0]

    user = await storage.get_user(identifier=user_id, context="auth")

    if not user:
        raise FailedLogin("Invalid token.")

    if not is_unverified_deletion_token and request.path == "/user/verify/delete":
        raise FailedLogin(
            "This endpoint requires an Unverified Account Deletion Token."
        )

    if user["verified"] and is_verification_token:
        raise FailedLogin("Verification tokens cannot be used more than once.")

    if user["verified"] and is_unverified_deletion_token:
        raise FailedLogin(
            "Unverified account deletion tokens can only be used while your account is pending approval."
        )

    if is_unverified_deletion_token and request.path != "/user/verify/delete":
        raise FailedLogin(
            "Unverified account deletion tokens cannot be used for authentication, but nice try."
        )

    if (
        (not user["verified"] and not is_verification_token)
        or (is_verification_token and request.path != "/user/verify")
    ) and not is_unverified_deletion_token:
        raise FailedLogin("User is not verified.")

    if is_password_reset_token and request.path != "/reset_password":
        raise FailedLogin(
            "Password reset tokens can only be used with the reset password endpoint."
        )

    if user["banned"]:
        raise FailedLogin("User is banned.")

    salt = user["password"]

    signer = TimestampSigner(app.api_config.TOKEN_SIGNING_KEY, salt=salt)

    token_age = (
        None
        if is_upload_token
        else app.api_config.VERIFICATION_TOKEN_AGE
        if is_verification_token
        else app.api_config.UNVERIFIED_ACCOUNT_DELETION_TOKEN_AGE
        if is_unverified_deletion_token
        else app.api_config.TIMED_TOKEN_AGE
    )

    try:
        if token_age is not None:
            signer.unsign(token, max_age=token_age)
        else:
            signer.unsign(token)
    except:
        raise FailedLogin("Invalid token.")

    return user_id
