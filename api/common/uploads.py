from quart import current_app as app


async def write_file_to_db(user, file_id, file_name, original_file_name, file_size):
    await app.db.execute(
        "INSERT INTO files (id, user_id, file_name, original_file_name, file_size) VALUES ($1, $2, $3, $4, $5)",
        file_id,
        user.get("id"),
        file_name,
        original_file_name,
        file_size,
    )


async def get_bucket(request):
    """Fetches the bucket from the request."""
    try:
        return request.args["bucket"]
    except KeyError:
        return app.api_config.S3_DEFAULT_BUCKET


async def check_bucket(request):
    """Fetches the bucket from the request and validates that the user has permission to upload to it."""
    bucket = await get_bucket(request)
    # TODO: APITHESQL-17 - Validate that the user has permission to upload to a given bucket
    return bucket
