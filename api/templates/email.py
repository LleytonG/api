from quart import current_app as app


VERIFICATION_EMAIL = """
Hey {username},
We received a request to sign up for an account for our service, {service_name}.

If this was you, please click the link below to verify your email address.
{base_url}/user/verify?token={token}

Thanks,
The team at {service_name}
"""
SIGNUP_ADMIN_APPROVAL_REQUIRED_EMAIL = """
Hey {username},

We received a request to sign up for an account for our service, {service_name}.

Currently, our systems are set so an administrator has to manually approve your account, as such, you will receive another e-mail once your account is approved for use.

If you did not initiate this request, please visit the link below, which will delete your account from our systems. Please note that the link is only valid for the next 48 hours, if you want to delete your account after the time is up, please email {support_email}.
{base_url}/user/verify/delete?token={token}

Thanks,
The team at {service_name}
"""

PASSWORD_RESET_EMAIL = """
Hey {username},

We received a request to reset your password on our service, {service_name}.

If this was you, please click the link below to initiate the password reset procedure.
<Normally, this is where a password reset frontend URL would be located, but as this is beta software, it requires that you make a POST request>

Reset token: {token}

Thanks,
The team at {service_name}
"""
