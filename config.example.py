HOST = "127.0.0.1"
PORT = 8080

SERVICE_NAME = ""
BASE_URL = ""
SUPPORT_EMAIL = ""

CLOUDFLARE_DCV_ENABLED = False  # Whether the Cloudflare Domain Control Validation Checker API is enabled. When disabled, the `dns` library is used, and resiliency against local DNS attacks is not provided.
CLOUDFLARE_DCV_AUTHTOKEN = ""

REGISTRATIONS_ENABLED = True
REGISTRATIONS_REQUIRE_APPROVAL = False

DATABASE_INFORMATION = {
    "host": "127.0.0.1",
    "port": 5432,
    "database": "api",
    "user": "api",
    "password": "",
    "min_size": 30,
    "max_size": 50,
    "max_inactive_connection_lifetime": 15,
}

TOKEN_SIGNING_KEY = ""
TIMED_TOKEN_AGE = 0
VERIFICATION_TOKEN_AGE = 0
UNVERIFIED_ACCOUNT_DELETION_TOKEN_AGE = 0

S3_INFORMATION = {
    "endpoint": "127.0.0.1:9000",
    "access_key": "",
    "secret_key": "",
    "secure": False,
}

S3_DEFAULT_BUCKET = ""

DEFAULT_UPLOAD_LIMIT = 100  # Maximum upload limit if not defined in a user's permission scheme; In megabytes. (n * 1024 * 1024 is what happens INTERNALLY)
MAXIMUM_SYSTEMWIDE_UPLOAD_LIMIT = 300  # Maximum upload limit for the ASGI server; In megabytes. (n * 1024 * 1024 is what happens INTERNALLY)

EMAIL_PROVIDER = ""
SEND_FROM_EMAIL = ""

SMTP_HOST = ""
SMTP_PORT = 465
SMTP_USERNAME = ""
SMTP_PASSWORD = ""

SENDGRID_API_KEY = ""
